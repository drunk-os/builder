#!/bin/bash

set -e

cd debs

notify(){
echo "##"
echo "# Drunk packing $PACKAGE_NAME"
echo "##"
}

sober(){
echo "##"
echo "# Packer got sober and all packages are at sdebs"
echo "##"
}

bdeb(){
PACKAGE_NAME=$1
notify
echo ""
dpkg-deb --build $1
echo ""
}

#
# Clean broken/unfinished/un-moved packages away
#

rm -f *.deb

#
# List of packages to build
#

bdeb base-system
bdeb bash
bdeb binutils
bdeb busybox
bdeb ca-certificate
bdeb dhcp
bdeb dhcpcd
bdeb drunk-base
bdeb drunk-live-base
bdeb e2fsprogs
bdeb efibootmgr
bdeb efivar
bdeb gcc
bdeb glibc
bdeb grub
bdeb kmod
bdeb linux-5.13.2
bdeb linux-modules-5.13.2
bdeb linux-headers-5.13.2
bdeb ncurses
bdeb popt
bdeb sed
bdeb sysvinit
bdeb tar
bdeb tcl
bdeb texinfo
bdeb triehash
bdeb unifont
bdeb util-linux
bdeb xz
bdeb zlib
bdeb zstd

#
# Make sdebs folder if already
#

mkdir -p ../debs/
mv *.deb ../debs/

#
# Get sober
#

sober
